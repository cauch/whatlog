#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#   Copyright (C) 2018 WhatLog
#   
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation; either version 2
#   of the License, or (at your option) any later version.
#   
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from PySide import QtCore, QtGui
import pickle
import sys
import os
import time
import random
import math
import copy
import re
from datetime import datetime

class Line:
    def __init__(self,number,raw,time_rules):
        self.raw = raw
        self.number = number
        self.visible = 1
        self.colors = {}
        self.clean = self.raw.strip()
        self.date = None
        self.apply_time_rules(time_rules)

    def apply_time_rules(self,time_rules):
        self.clean = self.raw.strip()
        self.date = None
        dateformat = time_rules.get("dateformat")
        t = ""
        try:
            t = self.clean
            if dateformat[0] != "%" and dateformat[-2] != "%":
                t = self.clean.split(dateformat[0])[1]
                t = t.split(dateformat[-1])[0]
                t = dateformat[0]+t+dateformat[-1]
            else:
                ns = dateformat.count(" ")
                t = " ".join(t.split()[:ns+1])
            datetime_object = datetime.strptime(t, dateformat)
            self.date = datetime_object
        except:
            pass
        if self.date is not None:
            dateformat = time_rules.get("reduce")
            if dateformat:
                t1 = self.date.strftime(dateformat)
                self.clean = self.clean.replace(t,t1)
                self.clean = eval(time_rules.get("reducerule"))
        
        
def is_true(value):
    if value in ["yes","y","Yes","Y","true","True","1",1,True]:
        return 1
    return 0        
        
class WhatLog(QtGui.QMainWindow):
    def __init__(self,app):
        self.app = app
        super(WhatLog, self).__init__()

        self.current_filename = ""
        self.current_cfgfn = None
        
        if len(sys.argv) > 1:
            self.current_filename = sys.argv[1]
        
        self.currentDialog = None
        
        self.color_rules = []
        self.color_rules_index = {}

        self.time_rules = {}
        self.time_rules["dateformat"] = "[%Y-%m-%d %H:%M:%S,"
        self.time_rules["reduce"] = "%d,%H:%M:%S"
        self.time_rules["reducerule"] = "self.clean.split(' ')[0][:-4]+' '+' '.join(self.clean.split(' ')[1:])"
        self.time_rules["dateformat"] = "%Y-%m-%d %H:%M:%S"
        self.time_rules["reduce"] = "%d,%H:%M:%S"
        self.time_rules["reducerule"] = "self.clean"
        
        self.line = []
        self.view_labels = []
        self.view_current = 0
        self.view_last = 0
        self.view_clickzones = {}
        self.view_x = 0

        self.bookmarks = [None]*10
        
        self.main = QtGui.QWidget()
        self.setCentralWidget(self.main)
        self.main.keyPressEvent = self.keyPressEvent
        
        #1) toolbar

        self.buttons = []
        button_prop = {}
        button_prop["cl"] = [u'cl','Colorize lines with patterns',0]
        button_prop["ti"] = [u'ti','Time related options and tools',0]
        button_prop["cf"] = [u'cf','Configuration profile',0]
        button_prop["na"] = [u'na','Navigate',0]
        # add search box

        self.bs = ["cf","cl","ti","na"]
        button_ico = []

        self.layout1 = QtGui.QHBoxLayout()
        self.layout1.setContentsMargins(5,0,0,0)
        self.layout1.setSpacing(12)

        self.quicksearch = QtGui.QLineEdit(self.main)
        #self.quicksearch.textChanged.connect(self.filterEditChanged)
        self.quicksearch.resize(100,18)
        self.quicksearch.setMaximumWidth(100)
        self.quicksearch.setMaximumHeight(18)
        self.quicksearch.show()
        self.quicksearch.clearFocus()
        self.layout1.addWidget(self.quicksearch,1)

        for i in range(len(self.bs)):
            p = button_prop[self.bs[i]]
            if os.path.exists( os.path.dirname(os.path.realpath(__file__))+"/img/"+self.bs[i]+".png" ) :
                self.buttons.append( QtGui.QPushButton('',self.main) )
                button_ico.append( QtGui.QIcon( QtGui.QPixmap( os.path.dirname(os.path.realpath(__file__))+"/img/"+self.bs[i]+".png") ) )
                self.buttons[-1].setIcon(button_ico[-1])
                self.buttons[-1].setIconSize(QtCore.QSize(16,16))
            else:
                self.buttons.append( QtGui.QPushButton(p[0],self.main) )
            self.buttons[-1].setToolTip(p[1])
            self.buttons[-1].resize(24,24)
            #self.buttons[-1].setFlat(True)
            self.buttons[-1].setMaximumWidth(24)
            self.buttons[-1].setMinimumWidth(24)
            self.buttons[-1].setMaximumHeight(24)
            if p[2] == 1:
                self.buttons[-1].setCheckable(True)
            self.buttons[-1].clicked.connect(lambda i=i: self.buttonsClick(i))
            self.buttons[-1].setFocus()
            self.layout1.addWidget(self.buttons[-1], i+2)
        self.layout1.addStretch(1)


        #2) display panel
        #3) color bar
        #4) scroll bar

        self.scene = QtGui.QGraphicsScene()
        self.view = QtGui.QGraphicsView(self.scene,self.main)
        self.view.setStyleSheet("background-color : whitesmoke; border:none;");
        policy1 = self.view.sizePolicy()
        policy1.setHorizontalStretch(90)
        self.view.setSizePolicy(policy1)
        self.view.resize(2000,3000)
        self.view.mousePressEvent = self.mousePressEvent_view
        self.view.wheelEvent = self.wheelEvent_view
        self.view.setMouseTracking(True)
        self.scene.mousePressEvent = self.mousePressEvent_view
        self.view.show()
        self.scrollbar_h = QtGui.QScrollBar(self.main,QtCore.Qt.Horizontal)
        self.scrollbar_h.setOrientation(QtCore.Qt.Horizontal)
        #policy4 = self.view.sizePolicy()
        #policy4.setVerticalStretch(1)
        #self.scrollbar_h.setSizePolicy(policy4)
        #self.scrollbar_h.setMaximumHeight(24)
        #self.scrollbar_h.setMinimumHeight(24)
        self.scrollbar_h.show()
        self.scrollbar_h.dont_change = 0
        self.scrollbar_h.connect(self.scrollbar_h, QtCore.SIGNAL("valueChanged(int)"), self.scrollbar_h_moved)

        layout4 = QtGui.QVBoxLayout()
        layout4.setContentsMargins(0,0,0,0)
        layout4.setSpacing(0)
        layout4.addWidget(self.view, 1)
        layout4.addWidget(self.scrollbar_h, 2)

        self.create_label_lines()
        
        self.colorbar = QtGui.QLabel("")
        self.colorbar_image = None #QtGui.QImage(10,200,QtGui.QImage.Format_ARGB32)
        self.colorbar_painter = None
        self.colorbar.setFixedWidth(40)
        policy4 = self.view.sizePolicy()
        policy4.setHorizontalStretch(90)
        self.colorbar.setSizePolicy(policy4)
        self.colorbar.mousePressEvent = self.mousePressEvent_colorbar
        self.colorbar.setStyleSheet("background-color : black; border:none;");
        #self.colorbar.setPixmap(QtGui.QPixmap(self.colorbar_image))
        
        #self.scene_clbar = QtGui.QGraphicsScene()
        #self.colorbar = QtGui.QGraphicsView(self.scene_clbar,self.main)
        #self.colorbar.setStyleSheet("background-color : black; border:none;");
        #policy2 = self.view.sizePolicy()
        #policy2.setHorizontalStretch(10)
        #self.colorbar.setSizePolicy(policy2)
        #self.colorbar.setMaximumWidth(24)
        #self.colorbar.setMinimumWidth(24)
        #self.scene_clbar.mousePressEvent = self.mousePressEvent_colorbar
        #self.colorbar.show()
        #self.colorbar.setMouseTracking(True)

        self.scrollbar_v = QtGui.QScrollBar(self.main,QtCore.Qt.Vertical)
        policy3 = self.view.sizePolicy()
        policy3.setHorizontalStretch(1)
        self.scrollbar_v.setSizePolicy(policy3)
        self.scrollbar_v.setMaximumWidth(24)
        self.scrollbar_v.setMinimumWidth(24)
        self.scrollbar_v.show()
        self.scrollbar_v.dont_change = 0
        self.scrollbar_v.time = time.time()
        self.scrollbar_v.connect(self.scrollbar_v, QtCore.SIGNAL("valueChanged(int)"), self.scrollbar_v_moved)

        self.scrollbar_v2 = QtGui.QScrollBar(self.main,QtCore.Qt.Vertical)
        policy32 = self.view.sizePolicy()
        policy32.setHorizontalStretch(1)
        self.scrollbar_v2.setSizePolicy(policy32)
        self.scrollbar_v2.setMaximumWidth(24)
        self.scrollbar_v2.setMinimumWidth(24)
        self.scrollbar_v2.setMaximum(1000)
        self.scrollbar_v2.setMinimum(0)
        self.scrollbar_v2.setValue(500)
        self.scrollbar_v2.show()
        #self.scrollbar_v2.mousePressEvent = self.scrollbar_v2_press
        self.scrollbar_v2.mouseReleaseEvent = self.scrollbar_v2_release
        self.scrollbar_v2.lastvalue = 500
        self.scrollbar_v2.connect(self.scrollbar_v2, QtCore.SIGNAL("valueChanged(int)"), self.scrollbar_v2_press)

        self.layout2 = QtGui.QHBoxLayout()
        self.layout2.setContentsMargins(2,0,0,2)
        self.layout2.setSpacing(0)
        self.layout2.addLayout(layout4, 1)
        self.layout2.addWidget(self.colorbar, 2)
        self.layout2.addWidget(self.scrollbar_v, 3)
        self.layout2.addWidget(self.scrollbar_v2, 4)

        #5) putting everything together

        self.layout3 = QtGui.QVBoxLayout(self.main)
        self.layout3.setContentsMargins(0,0,0,0)
        self.layout3.setSpacing(0)
        self.layout3.addLayout(self.layout1, 1)
        self.layout3.addLayout(self.layout2, 2)

        self.main.setLayout(self.layout3)

        #6) timer
        timer = QtCore.QTimer(self)
        self.connect(timer, QtCore.SIGNAL("timeout()"), self.time_update)
        timer.start(2000)
        
        self.setWindowState(QtCore.Qt.WindowMaximized)
        self.app.aboutToQuit.connect(self.myExitHandler)

    def closeEvent(self, event):
        self.current_data = []
        event.accept()
        
    def myExitHandler(self):
        self.current_data = []
        
    def init_colorbar(self):
        if self.colorbar_image is None:
            h = self.colorbar.height()
            self.colorbar_image = QtGui.QImage(40,h,QtGui.QImage.Format_ARGB32)
            if self.colorbar_painter is None:
                self.colorbar_painter = QtGui.QPainter(self.colorbar_image)
            self.colorbar_painter.setPen(QtGui.QPen(QtGui.QColor(0, 0, 0, 255)))
            self.colorbar_painter.setBrush(QtGui.QBrush(QtGui.QColor(0, 0, 0, 255)))
            self.colorbar_painter.drawRect(QtCore.QRect(0,0,40,h))
            self.colorbar_painter.setPen(QtGui.QPen(QtGui.QColor(100, 100, 100, 255)))
            self.colorbar_painter.drawLine(8,0,8,h)
            self.colorbar_painter.drawLine(42,0,42,h)

            # ###fffff|ccccccccccccccccccccccc|ccccccc
            
        else:
            if self.colorbar_painter is None:
                self.colorbar_painter = QtGui.QPainter(self.colorbar_image)
        
    def update_position_colorbar(self):
        self.init_colorbar()
        h = self.colorbar.height()
        total = len(self.lines)

        # red location
        f1 = self.view_current*h*1./total
        f2 = (self.view_last-1)*h*1./total
        self.colorbar_painter.setPen(QtGui.QPen(QtGui.QColor(0, 0, 0, 0)))
        self.colorbar_painter.setBrush(QtGui.QBrush(QtGui.QColor(0, 0, 0, 255)))
        self.colorbar_painter.drawRect(QtCore.QRect(0,0,3,h))
        self.colorbar_painter.setBrush(QtGui.QBrush(QtGui.QColor(255, 0, 0, 255)))
        dh = max(3,f2-f1)
        self.colorbar_painter.drawRect(QtCore.QRect(0,f1,3,dh))
        #self.colorbar_painter.end()

        # zoom on line mark
        self.colorbar_painter.setPen(QtGui.QPen(QtGui.QColor(0, 0, 0, 0)))
        self.colorbar_painter.setBrush(QtGui.QBrush(QtGui.QColor(0, 0, 0, 255)))
        self.colorbar_painter.drawRect(QtCore.QRect(17+8+8,0,7,h))
        n = int(h/2)
        past_lines = (-1*n)+20#-1*int(self.view_current*n*1./total)
        futur_lines = n+20#n+past_lines
        y = 0
        for i in range(past_lines,futur_lines):
            j = self.view_current+i
            if j < 0:
                y += 1
                continue
            if j >= len(self.lines):
                y += 1
                continue
            line = self.lines[j]
            cls = []
            if line is not None:
                cls = line.colors.get("dominant",[])
            width = 7
            for cl in cls:
                width += -1
                width = max(3,width)
                #self.colorbar_painter.setPen(QtGui.QPen(QtGui.QColor(cl)))
                self.colorbar_painter.setBrush(QtGui.QBrush(QtGui.QColor(cl)))
                self.colorbar_painter.drawRect(QtCore.QRect(17+8+8,y,width,3))
            y += 1
        
        self.colorbar_painter.setPen(QtGui.QPen(QtGui.QColor(255, 0, 0, 255)))
        self.colorbar_painter.drawLine(17+8+8,h/2,24+16,h/2)

        self.colorbar.setPixmap(QtGui.QPixmap(self.colorbar_image))
        
    def time_update(self):
        h = self.colorbar.height()
        total = len(self.lines)
        f1 = self.view_current*h*1./total
        f2 = self.view_last*h*1./total
        self.colorbar_painter.setPen(QtGui.QPen(QtGui.QColor(255, 255, 255, 0)))
        dh = max(3,f2-f1)
        self.colorbar_painter.setBrush(QtGui.QBrush(QtGui.QColor(255, 255, 255, 5)))
        self.colorbar_painter.drawRect(QtCore.QRect(3,f1,1,dh))
        self.colorbar_painter.setBrush(QtGui.QBrush(QtGui.QColor(255, 255, 255, 4)))
        self.colorbar_painter.drawRect(QtCore.QRect(4,f1,1,dh))
        self.colorbar_painter.setBrush(QtGui.QBrush(QtGui.QColor(255, 255, 255, 3)))
        self.colorbar_painter.drawRect(QtCore.QRect(5,f1,1,dh))
        self.colorbar_painter.setBrush(QtGui.QBrush(QtGui.QColor(255, 255, 255, 2)))
        self.colorbar_painter.drawRect(QtCore.QRect(6,f1,1,dh))
        self.colorbar_painter.setBrush(QtGui.QBrush(QtGui.QColor(255, 255, 255, 1)))        
        self.colorbar_painter.drawRect(QtCore.QRect(7,f1,1,dh))
        #self.colorbar_painter.end()
        self.colorbar.setPixmap(QtGui.QPixmap(self.colorbar_image))

    def create_label_lines(self):
        self.view_labels = []
        y = 0
        desk = QtGui.QDesktopWidget()
        y_max = desk.availableGeometry().height()-30
        while y < y_max:
            t = "%i"%(len(self.view_labels))
            lo1 = QtGui.QLabel(t,self.view)
            lo1.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse)
            lo1.setMouseTracking(True)
            lo1.show()
            lo1.move(1,1+y)
            #lo1.mousePressEvent = self.mousePressEvent_view
            lo1.mouseDoubleClickEvent = self.labelnum_doubleclick
            lo1.linenumber = -1
            lo2 = QtGui.QLabel(t,self.view)
            lo2.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse)
            lo2.wheelEvent = self.wheelEvent_view
            lo2.setMouseTracking(True)
            lo2.show()
            lo2.move(60,1+y)
            #self.connect(lo2,QtCore.SIGNAL("doubleClicked()"),self.label_doubleclick)
            #lo2.clicked.connect(self.label_doubleclick)
            lo2.mouseDoubleClickEvent = self.label_doubleclick
            y+=15
            self.view_labels.append((lo1,lo2))

    def label_doubleclick(self,a):
        item = self.view.focusWidget()
        msgBox = QtGui.QMessageBox()
        msgBox.setStyleSheet("QPushButton{min-width: 1000px;}")
        msgBox.setGeometry(10,10,1000,500)
        line = item.line
        text = line.raw
        number = line.number
        while len(text) < 5000:
            number += 1
            if number >= len(self.lines):
                break
            text += self.lines[number].raw 
        msgBox.setInformativeText(text)
        msgBox.exec_()

    def labelnum_doubleclick(self,a):
        item = self.view.focusWidget()
        ln = item.linenumber
        if ln in self.bookmarks:
            ind_ = self.bookmarks.index(ln)
            self.bookmarks[ind_] = None
        elif None in self.bookmarks:
            ind_ = self.bookmarks.index(None)
            self.bookmarks[ind_] = ln
        self.display()
            
    def scrollbar_h_moved(self,changed):
        if self.scrollbar_h.dont_change:
            return
        pos = self.scrollbar_h.sliderPosition()
        self.view_x = -1*pos
        self.display()
        
    def scrollbar_v_moved(self,changed):
        if self.scrollbar_v.dont_change:
            return
        line_i = self.scrollbar_v.sliderPosition()
        if line_i < len(self.scrollbar_v.lines):
            line = self.scrollbar_v.lines[line_i]
            self.view_current = line
            self.display()

    def scrollbar_v2_press(self,e):
        i = self.scrollbar_v2.sliderPosition()
        delta = i-self.scrollbar_v2.lastvalue
        self.scrollbar_v2.lastvalue = i
        delta = int(delta/2)
        line_i = self.scrollbar_v.sliderPosition()
        if line_i+delta < len(self.scrollbar_v.lines) and line_i+delta > 0:
            line = self.scrollbar_v.lines[line_i+delta]
            self.view_current = line
            self.display()

    def scrollbar_v2_release(self,e):
        self.scrollbar_v2.lastvalue = 500
        self.scrollbar_v2.setValue(500)
            
    def cfgsave(self,filename):
        dico = {}
        dico["color_rules"] = self.color_rules
        dico["time_rules"] = self.time_rules
        pickle.dump( dico, open( filename, "wb" ) )

    def cfgload(self,filename):
        dico = pickle.load( open( filename, "rb" ) )
        self.color_rules = dico["color_rules"]
        self.time_rules = dico["time_rules"]
        self.update_color()
        self.display()
        
    def keyPressEvent(self,t):
        #if t.key()==QtCore.Qt.Key_Return:
        #    self.listViewClick(True)
        #if t.key()==QtCore.Qt.Key_Up or t.key()==QtCore.Qt.Key_Down:
        #    self.listViewClick(False)
        bookmark_keys = [QtCore.Qt.Key_1,QtCore.Qt.Key_2,QtCore.Qt.Key_3,
                         QtCore.Qt.Key_4,QtCore.Qt.Key_5,QtCore.Qt.Key_6,
                         QtCore.Qt.Key_7,QtCore.Qt.Key_8,QtCore.Qt.Key_9,
                         QtCore.Qt.Key_0]
        if t.key() in bookmark_keys:
            ind_ = bookmark_keys.index(t.key())
            if self.bookmarks[ind_] is not None:
                self.view_current = self.bookmarks[ind_]
                self.display()
        if t.key()==QtCore.Qt.Key_A:            
            line_i = self.scrollbar_v.sliderPosition()
            if line_i+5 < len(self.scrollbar_v.lines):
                line = self.scrollbar_v.lines[line_i+5]
                self.view_current = line
                self.display()
        if t.key()==QtCore.Qt.Key_S:            
            line_i = self.scrollbar_v.sliderPosition()
            if line_i-5 > 0:
                line = self.scrollbar_v.lines[line_i-5]
                self.view_current = line
                self.display()
        if t.key()==QtCore.Qt.Key_Escape:
            self.close()
        if t.key()==QtCore.Qt.Key_W:
            self.close()
        
    def mousePressEvent_view(self,mouse):
        if mouse.button() == QtCore.Qt.LeftButton:
            pos = self.view.mapFromGlobal(mouse.globalPos())
            print "click",pos.x(),pos.y()
            for l in self.view_clickzones:
                x,y = [int(_) for _ in l[1:-1].split(",")]
                print l,x,y
                if (pos.x()-x)**2 + (pos.y()-y)**2 < 25:
                    self.hasBeenClicked(self.view_clickzones[l])

    def wheelEvent_view(self,wheel):
        delta = wheel.delta() 
        if abs(delta) <= 5:
            return
        if time.time() - self.scrollbar_v.time < 0.25:
            return
        self.scrollbar_v.time = time.time()
        ampl = 2
        if abs(delta) > 15:
            ampl = 5
        if abs(delta) > 20:
            ampl = 20
        #print "delta",delta
        if delta > 0:
            self.scrollbar_v.setValue(self.scrollbar_v.sliderPosition() - ampl)
        if delta < 0:
            self.scrollbar_v.setValue(self.scrollbar_v.sliderPosition() + ampl)
        #print delta
                    
    def hasBeenClicked(self,l):
        if l[0] == "arrow":
            self.display()

    def mousePressEvent_colorbar(self,mouse):
        if mouse.button() == QtCore.Qt.LeftButton:
            x,y = mouse.x(),mouse.y()
            if x >= 33:
                h = self.colorbar.height()
                n = int(h/2)
                past_lines = (-1*n)+20
                futur_lines = n+20
                y2 = 0
                i2 = 0
                for i in range(past_lines,futur_lines):
                    j = self.view_current+i
                    if j < 0:
                        y2 += 1
                        continue
                    if j >= len(self.lines):
                        y2 += 1
                        continue
                    i2 = j
                    if y2+10>=y:
                        break
                    y2 += 1
                self.view_current = i2
                self.display()
            else:
                h = self.colorbar.height()
                j = int(len(self.lines)*(y-2)*1./h)
                self.view_current = j
                self.display()
        
    def buttonsClick(self,i):
        global history,currentPath,currentFile
        if self.bs[i] == "":
            print ""
        elif self.bs[i] in ["cf","cl","na","ti"]:
            if self.currentDialog:
                pass#a dialog is open
            else:
                if self.bs[i] == "cl":
                    self.currentDialog = ColDlg(self)
                if self.bs[i] == "na":
                    self.currentDialog = NavDlg(self)
                if self.bs[i] == "cf":
                    self.currentDialog = CfgDlg(self)
                if self.bs[i] == "ti":
                    self.currentDialog = TimeDlg(self)
                self.currentDialog.run()

    def process_file(self):
        if self.current_data_i < len(self.current_data):
            i = self.current_data_i
            d = self.current_data[i]
            if i%10000==0:
                print i,"-",len(self.current_data)
            line = Line(i,d,self.time_rules)
            self.lines[i] = line            
            self.current_data_i += 1
            return 1
        return 0
            
    def load_file(self):
        if not os.path.exists(self.current_filename) or not os.path.isfile(self.current_filename):
            return
        self.current_file = open(self.current_filename,"r")
        self.current_data = self.current_file.readlines()
        self.current_data_i = 0
        self.lines = []
        self.scrollbar_v.lines = []
        N = len(self.current_data)
        self.lines = [None for i in range(N)]
        self.scrollbar_v.lines = list(range(N))
        for i in range(N):#min(int(N/4),N)):
            self.process_file()
            
        title = self.current_filename
        if len(title) > 107:
            title = "..."+title[-107:]
        self.setWindowTitle("WhatLog - %s"%title)

        self.display()

        if self.current_filename:
            current_ = os.path.basename(self.current_filename)
            listfile = os.listdir(os.path.join(os.path.dirname(os.path.realpath(__file__)),"cfg"))
            l = [_ for _ in listfile if _[:-4] in current_]
            if len(l) == 1:
                fn = l[0]
                filename = os.path.dirname(os.path.realpath(__file__))+"/cfg/"+fn
                self.cfgload(filename)

        while self.process_file():
            self.app.processEvents()
            if self.current_data_i % 10001 == 0:
                self.update_color()
                
        self.update_color()
        self.display()
        self.scrollbar_v.setMaximum(len(self.scrollbar_v.lines))
        self.scrollbar_v.setMinimum(0)

    def compare(self,pattern,text,regex):
        if not pattern:
            return 0
        if regex:
            p = re.compile(pattern)
            if p.match(text):
                return 1
        else:
            if pattern in text:
                return 1
        return 0

    def update_color(self):
        er = []
        for r in self.color_rules:
            ignore = is_true(r.get("ignore",True))
            if ignore:
                continue
            pattern = r.get("pattern")
            if pattern is None:
                continue
            priority = r.get("priority",0)
            er.append((priority,r))
        er.sort()
        er = [_[1] for _ in er]
            
        h = self.colorbar.height()
        total = len(self.lines)
        self.colorbar_painter.setPen(QtGui.QPen(QtGui.QColor(0, 0, 0, 255)))
        self.colorbar_painter.setBrush(QtGui.QBrush(QtGui.QColor(0, 0, 0, 255)))
        self.colorbar_painter.drawRect(QtCore.QRect(9,0,6+8+8,h))

        self.scrollbar_v.lines = []
        coloring = {}
        self.color_rules_index = {}
        for line in self.lines:
            if line is None:
                continue
            line.visible = 1
            line.colors = {}
            for eri,r in enumerate(er):
                pat = r.get("pattern")
                reg = is_true(r.get("regex",True))
                if self.compare(pat,line.clean,reg):
                    if r.get("name") not in self.color_rules_index:
                        self.color_rules_index[r.get("name")] = []
                    self.color_rules_index[r.get("name")].append(line.number)
                    if is_true(r.get("hide",False)):
                        line.visible = 0
                    cl = r.get("color")
                    if cl is not None:
                        if r.get("mode") == "text":
                            line.colors["body"] = cl
                        if r.get("mode") == "background":
                            line.colors["bkgd"] = cl
                        if r.get("mode") == "line number":
                            line.colors["head"] = cl
                        if "dominant" not in line.colors:
                            line.colors["dominant"] = []
                        line.colors["dominant"].append(cl)
                        f1 = line.number*h*1./total
                        try:
                            coloring[eri].append([f1,cl])
                        except KeyError:
                            coloring[eri] = [[f1,cl]]
            if line.visible:
                self.scrollbar_v.lines.append(line.number)

        keys = coloring.keys()
        keys.sort()
        width = 7+8+8
        for k in keys:
            width += -3
            width = max(3,width)
            for f1,cl in coloring[k]:
                self.colorbar_painter.setPen(QtGui.QPen(QtGui.QColor(cl)))
                self.colorbar_painter.setBrush(QtGui.QBrush(QtGui.QColor(cl)))
                self.colorbar_painter.drawRect(QtCore.QRect(9,f1,width,1))

                
    def display(self):
        total = len(self.lines)
        max_length = 0
        i = 0
        j = self.view_current+0
        self.view_clickzones = {}
        while self.lines[j].visible == 0:
            j += -1
            if j == 0:
                break
        #    bci = self.lines[j].box_contained_in
        #    if bci == -1:
        #        break
        #    j = bci
        x0 = self.view_x
        first = j+0
        la1width = None
        previoustime = None
        while i < len(self.view_labels):
            la1 = self.view_labels[i][0]
            la2 = self.view_labels[i][1]
            i += 1
            if j >= len(self.lines):
                l = None
            else:
                l = self.lines[j]
            while l is not None and l.visible == 0:
                j += 1
                if j >= len(self.lines):
                    l = None
                else:
                    l = self.lines[j]
            if l is None:
                la1.setText("")
                la2.setText("")
                la1.setStyleSheet("")
                la2.setStyleSheet("")                
                continue
            t = "<html><body>"
            t += "<tt>"
            
            lns = 1+int(math.log10(total))
            ln = str(l.number+1).zfill(lns)
            if (l.number+1)%10 == 0:
                t += "<font color='magenta' size=1>%s </font>"%ln
            else:
                t += "<font color='grey' size=1>%s </font>"%ln
            ts = "&#9474;"
            # unicode numbers
            loun = ["&#10112;","&#10113;","&#10114;","&#10115;",
                    "&#10116;","&#10117;","&#10118;","&#10119;",
                    "&#10120;","&#10121;","&#10122;"]
            cl = "black"
            if previoustime is not None and l.date is not None:
                if (l.date-previoustime).total_seconds() > 1:
                    cl = "#AA0000"
                if (l.date-previoustime).total_seconds() > 5:
                    cl = "#AAAA00"
                if (l.date-previoustime).total_seconds() > 30:
                    cl = "#AA00AA"                    
            previoustime = l.date
            if l.number in self.bookmarks:
                ts = loun[self.bookmarks.index(l.number)]
                cl = "orange"
            t += "<font color='%s'>%s </font>"%(cl,ts)
            t += "</tt>"
            t += "</body></html>"
            la1.move(1+x0,la1.pos().y())
            la1.setText(t)
            la1.adjustSize()
            la1.linenumber = l.number
            self.view_last = l.number
            if la1width is None:
                la1width = la1.width()
            la2.move(1+x0+la1width,la2.pos().y())
            t = "<html><body>"
            t += "<tt>"
            t += l.clean
            t += "</tt>"
            t += "</body></html>"
            la2.line = l
            la2.setText(t)
            la1.setStyleSheet("")
            la2.setStyleSheet("")
            if l.colors.get("body") and l.colors.get("bkgd"):
                la2.setStyleSheet("QLabel { background-color :"+l.colors.get("bkgd")+" ; color : "+l.colors.get("body")+"; }")
            elif l.colors.get("body"):
                la2.setStyleSheet("QLabel { color : "+l.colors.get("body")+"; }")
            elif l.colors.get("bkgd"):
                la2.setStyleSheet("QLabel { background-color :"+l.colors.get("bkgd")+" ; }")
            if l.colors.get("head"):
                la1.setStyleSheet("QLabel { background-color :"+l.colors.get("head")+" ; }")
            la2.adjustSize()
            if la1.width()+la2.width() > max_length:
                max_length = la1.width()+la2.width()
            if 0:
                x,y = la1.pos().x(),la1.pos().y()
                self.view_clickzones["(%i,%i)"%(x+27,y+10)] = ["arrow",l]
            j += 1
        self.view_current = first
        self.update_position_colorbar()
        self.scrollbar_v.dont_change = 1
        self.scrollbar_v.setMaximum(len(self.scrollbar_v.lines))
        self.scrollbar_v.setMinimum(0)
        self.scrollbar_v.setValue(self.scrollbar_v.lines.index(first))
        self.scrollbar_v.dont_change = 0
        self.scrollbar_h.dont_change = 1
        self.scrollbar_h.setMaximum(max(0,max_length-self.view.width()))        
        self.scrollbar_h.dont_change = 0
            
    def about(self):
        QtGui.QMessageBox.about(self, "About WhatLog",
                "<p><b>watlog.py</b> is just a tool to play around with logs.</p>")

    def createActions(self):
        self.openAct = QtGui.QAction("&Open...", self, shortcut="Ctrl+O",
                triggered=self.open)

        self.exitAct = QtGui.QAction("E&xit", self, shortcut="Ctrl+Q",
                triggered=self.close)

        self.aboutAct = QtGui.QAction("&About", self, triggered=self.about)

        self.aboutQtAct = QtGui.QAction("About &Qt", self,
                triggered=QtGui.qApp.aboutQt)

    def closeDialog(self,evt):
        self.currentDialog = None
        
    def closeEvent(self, event):
        event.accept() # let the window close


#different tools
class QColorButton(QtGui.QPushButton):
    '''
    Custom Qt Widget to show a chosen color.

    From https://www.pymadethis.com/article/qcolorbutton-a-color-selector-tool-for-pyqt/

    Left-clicking the button shows the color-chooser, while
    right-clicking resets the color to None (no-color).    
    '''

    def __init__(self, *args, **kwargs):
        super(QColorButton, self).__init__(*args, **kwargs)
        self._color = None
        self.setMaximumWidth(32)
        self.setMaximumHeight(24)
        self.pressed.connect(self.onColorPicker)

    def setColor(self, color):
        if color != self._color:
            self._color = color

        if self._color:
            self.setStyleSheet("background-color: %s;" % self._color)
        else:
            self.setStyleSheet("")

    def color(self):
        return self._color

    def onColorPicker(self):
        '''
        Show color-picker dialog to select color.

        Qt will use the native dialog by default.

        '''
        dlg = QtGui.QColorDialog(self)
        dlg.setStyleSheet("background-color: lightgrey;")
        
        if self._color:
            dlg.setCurrentColor(QtGui.QColor(self._color))

        if dlg.exec_():
            self.setColor(dlg.currentColor().name())

    def mousePressEvent(self, e):
        if e.button() == QtCore.Qt.RightButton:
            self.setColor(None)

        return super(QColorButton, self).mousePressEvent(e)

class DummyDlg:
    def __init__(self,main):
        self.main = main
        self.window = QtGui.QDialog(main)
        self.window.show()
        self.window.setWindowTitle("WhatLog")
        self.window.setWindowFlags( QtCore.Qt.Dialog | QtCore.Qt.WindowStaysOnTopHint )
        self.type_ = ""
        self.objects = {}
        self.layouts = {}
        self.config = []

    def add_form(self):
        for co in self.config:
            va = co["id"]
            te = co.get("te","")
            ty = co.get("ty")
            if ty == "e": # edit
                self.objects["l_"+va] = QtGui.QLabel(self.window)
                self.objects["l_"+va].setText(te)
                self.objects["l_"+va].show()
                self.objects["e_"+va] = QtGui.QLineEdit(self.window)
                self.objects["e_"+va].show()
                self.layouts[va] = QtGui.QHBoxLayout()
                self.layouts[va].addWidget(self.objects["l_"+va],1)
                self.layouts[va].addWidget(self.objects["e_"+va],2)
            if ty == "x": # checkbox
                self.objects["x_"+va] = QtGui.QCheckBox(te,self.window)
                self.objects["x_"+va].show()
                self.layouts[va] = QtGui.QHBoxLayout()
                self.layouts[va].addWidget(self.objects["x_"+va],1)
            if ty == "l": # combobox
                self.objects["la_"+va] = QtGui.QLabel(self.window)
                self.objects["la_"+va].setText(te)
                self.objects["la_"+va].show()
                self.objects["li_"+va] = QtGui.QComboBox(self.window)
                for _ in co.get("ch",[]):
                    self.objects["li_"+va].addItem(_)
                self.objects["li_"+va].show()
                self.layouts[va] = QtGui.QHBoxLayout()
                self.layouts[va].addWidget(self.objects["la_"+va],1)
                self.layouts[va].addWidget(self.objects["li_"+va],2)
            if ty == "c": # color
                self.objects["l_"+va] = QtGui.QLabel(self.window)
                self.objects["l_"+va].setText(te)
                self.objects["l_"+va].show()
                self.objects["c_"+va] = QColorButton(self.window)
                self.objects["c_"+va].show()
                self.layouts[va] = QtGui.QHBoxLayout()
                self.layouts[va].addWidget(self.objects["l_"+va],1)
                self.layouts[va].addWidget(self.objects["c_"+va],2)
                self.layouts[va].addStretch(1)
            if ty == "n": # spin box
                self.objects["l_"+va] = QtGui.QLabel(self.window)
                self.objects["l_"+va].setText(te)
                self.objects["l_"+va].show()
                self.objects["n_"+va] = QtGui.QSpinBox(self.window)
                self.objects["n_"+va].show()
                self.layouts[va] = QtGui.QHBoxLayout()
                self.layouts[va].addWidget(self.objects["l_"+va],1)
                self.layouts[va].addWidget(self.objects["n_"+va],2)

    def randomcl(self):
        return QtGui.QColor(random.randint(0,255), random.randint(0,255), random.randint(0,255), 255).name()
                
    def fill_form(self,value):
        for co in self.config:
            name = co["id"]
            ty = co["ty"]
            va = value.get(name)
            if va is None:
                va = {"e":"","x":False,"c":self.randomcl(),"n":0,"l":""}[ty]
                if name == "ignore":
                    va = True
            if ty == "e":
                self.objects["e_"+name].setText(va)
            elif ty == "x":
                self.objects["x_"+name].setChecked(is_true(va))
            elif ty == "c":
                self.objects["c_"+name].setColor(va)        
            elif ty == "l":
                ind_ = 0
                if va in co["ch"]:
                    ind_ = co["ch"].index(va)
                self.objects["li_"+name].setCurrentIndex(ind_)
            elif ty == "n":
                self.objects["n_"+name].setValue(int(va))

    def return_form(self):
        r = {}
        for co in self.config:
            name = co["id"]
            ty = co["ty"]
            if ty == "e":
                obj = self.objects["e_"+name]
                r[name] = obj.text()
            elif ty == "x":
                obj = self.objects["x_"+name]
                r[name] = obj.isChecked()
            elif ty == "c":
                obj = self.objects["c_"+name]
                r[name] = obj.color()
            elif ty == "l":
                obj = self.objects["li_"+name]
                r[name]= obj.currentText()
            elif ty == "n":
                obj = self.objects["n_"+name]
                r[name]= obj.value()
        return r

    def apply_change(self):
        pass
    
    def close(self):
        if self.change:
            self.apply_change()
        self.window.close()
        

    def run(self):
        self.window.exec_()

class ColDlg(DummyDlg):
    def __init__(self,main):
        DummyDlg.__init__(self,main)

        self.change = 0
        self.type_ = "cl"

        self.current_rules = self.main.color_rules[:]
        self.current_name = ""
        
        self.combobox = QtGui.QComboBox(self.window)
        self.combobox.show()
        for box_rule in self.current_rules:
            name = box_rule.get("name","Error: no name")
            self.combobox.addItem(name)
        self.combobox.setCurrentIndex(-1)

        self.combobox.currentIndexChanged.connect(self.indexChanged)

        self.objects = {}
        self.layouts = {}

        self.config.append( {"id":"name",
                             "te":"Name:",
                             "ty":"e"} )
        self.config.append( {"id":"pattern",
                             "te":"Pattern:",
                             "ty":"e"} )
        self.config.append( {"id":"priority",
                             "te":"Priority:",
                             "ty":"n"} )
        self.config.append( {"id":"regex",
                             "te":"Regex expression",
                             "ty":"x"} )
        self.config.append( {"id":"ignore",
                             "te":"This rule is IGNORED",
                             "ty":"x"} )
        self.config.append( {"id":"mode",
                             "te":"Apply color to",
                             "ch":["background","text","line number"],
                             "ty":"l"} )
        self.config.append( {"id":"hide",
                             "te":"Hide the line",
                             "ty":"x"} )
        self.config.append( {"id":"color",
                             "te":"Color of the line",
                             "ty":"c"} )
        
        self.add_form()

        self.buttonsave = QtGui.QPushButton("save current entry",self.window)
        self.buttonsave.show()
        self.buttonsave.clicked.connect(self.rule_save)
        self.buttonnew = QtGui.QPushButton("new entry",self.window)
        self.buttonnew.show()
        self.buttonnew.clicked.connect(self.rule_new)
        self.buttondelete = QtGui.QPushButton("delete current entry",self.window)
        self.buttondelete.show()
        self.buttondelete.clicked.connect(self.rule_delete)
        self.buttonclose = QtGui.QPushButton("close",self.window)
        self.buttonclose.show()
        self.buttonclose.clicked.connect(self.close)
        layout2 = QtGui.QHBoxLayout()
        layout2.addWidget(self.buttonsave)
        layout2.addWidget(self.buttonnew)
        layout2.addWidget(self.buttondelete)
        layout2.addWidget(self.buttonclose)
        layout2.addStretch(1)

        self.layout3 = QtGui.QVBoxLayout(self.window)
        self.layout3.setContentsMargins(1,1,1,1)
        #self.layout3.setSpacing(0)
        self.layout3.addWidget(self.combobox)
        for va in [_["id"] for _ in self.config]:
            self.layout3.addLayout(self.layouts[va])
        self.layout3.addLayout(layout2)
        self.layout3.addStretch(1)

        self.window.resize(850, 300)
        self.window.closeEvent = self.main.closeDialog

    def apply_change(self):
        self.main.update_color()
        self.main.display()
        
    def indexChanged(self,i):
        name = self.combobox.itemText(i)
        rule = [_ for _ in self.current_rules if _["name"] == name][0]
        self.current_name = name
        self.fill_form(rule)

    def rule_save(self):
        self.change = 1
        name = self.current_name
        name_index = [i for i,r in enumerate(self.current_rules) if r["name"] == name][0]
        self.current_rules[name_index] = self.return_form()
        name_index = [i for i,r in enumerate(self.main.color_rules) if r["name"] == name]
        if name_index:
            self.main.color_rules[name_index[0]] = self.return_form()
        else:
            self.main.color_rules.append(self.return_form())
            
    def rule_delete(self):
        name = self.current_name
        name_index = [i for i,r in enumerate(self.current_rules) if r["name"] == name]
        if not name_index:
            return
        del self.current_rules[name_index[0]]
        name_index = [i for i,r in enumerate(self.main.color_rules) if r["name"] == name]
        if name_index:
            self.change = 1
            del self.main.color_rules[name_index[0]]

    def rule_new(self):
        existingnames = [_["name"] for _ in self.current_rules]
        i = 1
        nname = "New_%03i"%i
        while nname in existingnames:
            i += 1
            nname = "New_%03i"%i
        self.current_rules.append({"name":nname,"ignore":1})
        self.combobox.addItem(nname)
        i = 0
        while i<1000:
            name = self.combobox.itemText(i)
            if name == nname:
                break
            i+=1
        self.combobox.setCurrentIndex(i)

class NavDlg(DummyDlg):
    def __init__(self,main):
        DummyDlg.__init__(self,main)

        self.change = 0
        self.type_ = "na"

        self.current_rules = self.main.color_rules[:]
        self.current_name = ""
        
        self.combobox = QtGui.QComboBox(self.window)
        self.combobox.show()
        for box_rule in self.current_rules:
            name = box_rule.get("name","Error: no name")
            self.combobox.addItem(name)
        self.combobox.setCurrentIndex(-1)

        self.combobox.currentIndexChanged.connect(self.indexChanged)

        self.objects = {}
        self.layouts = {}

        self.config.append( {"id":"color",
                             "te":"Color of the line",
                             "ty":"c"} )
        
        self.add_form()
        
        self.buttonprev = QtGui.QPushButton("previous",self.window)
        self.buttonprev.show()
        self.buttonprev.clicked.connect(self.nav_prev)
        self.buttonnext = QtGui.QPushButton("next",self.window)
        self.buttonnext.show()
        self.buttonnext.clicked.connect(self.nav_next)
        layout2 = QtGui.QHBoxLayout()
        layout2.addWidget(self.buttonprev)
        layout2.addWidget(self.buttonnext)
        layout2.addStretch(1)

        self.layout3 = QtGui.QVBoxLayout(self.window)
        self.layout3.setContentsMargins(1,1,1,1)
        #self.layout3.setSpacing(0)
        self.layout3.addWidget(self.combobox)
        for va in [_["id"] for _ in self.config]:
            self.layout3.addLayout(self.layouts[va])
        self.layout3.addLayout(layout2)
        self.layout3.addStretch(1)

        self.window.resize(850, 100)
        self.window.closeEvent = self.main.closeDialog

    def apply_change(self):
        self.main.display()
        
    def indexChanged(self,i):
        name = self.combobox.itemText(i)
        rule = [_ for _ in self.current_rules if _["name"] == name][0]
        self.current_name = name
        self.fill_form(rule)
        
    def nav_next(self):
        name = self.current_name
        if name in self.main.color_rules_index:
            list_ = self.main.color_rules_index[name]
            list_.sort()
            list_ = [_ for _ in list_ if _ > self.main.view_current]
            if len(list_)>1:
                self.main.view_current = max(list_[1]-5,0)
                self.main.display()
        
    def nav_prev(self):
        name = self.current_name
        if name in self.main.color_rules_index:
            list_ = self.main.color_rules_index[name]
            list_.sort()
            list_ = [_ for _ in list_ if _ < self.main.view_current+5]
            if list_:
                self.main.view_current = max(list_[-1]-5,0)
                self.main.display()


class TimeDlg(DummyDlg):
    def __init__(self,main):
        DummyDlg.__init__(self,main)

        self.change = 0
        self.type_ = "ti"

        current_line = self.main.lines[self.main.view_current]
        current_date = datetime.now()
        if current_line.date:
            current_date = current_line.date
        
        self.c_day = QtGui.QComboBox(self.window)
        for _ in range(31):
            self.c_day.addItem("%02i"%(_+1))
        self.c_day.setMaximumWidth(46)
        self.c_day.setMinimumWidth(46)
        self.c_day.setMaximumHeight(24)
        self.c_day.setToolTip("day")
        self.c_day.setStyleSheet("border:none;")
        self.c_day.show()
        self.c_day.setCurrentIndex(current_date.day-1)

        self.c_mon = QtGui.QComboBox(self.window)
        for _ in range(12):
            self.c_mon.addItem("%02i"%(_+1))
        self.c_mon.setMaximumWidth(46)
        self.c_mon.setMinimumWidth(46)
        self.c_mon.setMaximumHeight(24)
        self.c_mon.setToolTip("month")
        self.c_mon.setStyleSheet("border:none;")
        self.c_mon.show()
        self.c_mon.setCurrentIndex(current_date.month-1)

        self.c_yea = QtGui.QComboBox(self.window)
        current_year = current_date.year
        for _ in range(9):
            self.c_yea.addItem("%i"%(current_year-4+_))
        self.c_yea.setMaximumWidth(60)
        self.c_yea.setMinimumWidth(60)
        self.c_yea.setMaximumHeight(24)
        self.c_yea.setToolTip("year")
        self.c_yea.setStyleSheet("border:none;")
        self.c_yea.show()
        self.c_yea.setCurrentIndex(4)

        l_yh = QtGui.QLabel(self.window)
        l_yh.setText(" ")
        l_yh.setMaximumWidth(6)
        l_yh.setMinimumWidth(6)
        l_yh.show()

        self.c_hou = QtGui.QComboBox(self.window)
        for _ in range(24):
            self.c_hou.addItem("%02i"%(_))
        self.c_hou.setMaximumWidth(46)
        self.c_hou.setMinimumWidth(46)
        self.c_hou.setMaximumHeight(24)
        self.c_hou.setToolTip("hour")
        self.c_hou.setStyleSheet("border:none;")
        self.c_hou.show()
        self.c_hou.setCurrentIndex(current_date.hour-1)

        self.c_min = QtGui.QComboBox(self.window)
        for _ in range(60):
            self.c_min.addItem("%02i"%(_))
        self.c_min.setMaximumWidth(46)
        self.c_min.setMinimumWidth(46)
        self.c_min.setMaximumHeight(24)
        self.c_min.setToolTip("minute")
        self.c_min.setStyleSheet("border:none;")
        self.c_min.show()
        self.c_min.setCurrentIndex(current_date.minute-1)

        self.c_sec = QtGui.QComboBox(self.window)
        for _ in range(60):
            self.c_sec.addItem("%02i"%(_))
        self.c_sec.setMaximumWidth(46)
        self.c_sec.setMinimumWidth(46)
        self.c_sec.setMaximumHeight(24)
        self.c_sec.setToolTip("second")
        self.c_sec.setStyleSheet("border:none;")
        self.c_sec.show()
        self.c_sec.setCurrentIndex(current_date.second-1)

        
        self.objects = {}
        self.layouts = {}
        
        self.buttongo = QtGui.QPushButton("go",self.window)
        self.buttongo.show()
        self.buttongo.clicked.connect(self.nav_go)
        layout2 = QtGui.QHBoxLayout(self.window)
        layout2.addWidget(self.c_day)
        layout2.addWidget(self.c_mon)
        layout2.addWidget(self.c_yea)
        layout2.addWidget(l_yh)
        layout2.addWidget(self.c_hou)
        layout2.addWidget(self.c_min)
        layout2.addWidget(self.c_sec)        
        layout2.addWidget(self.buttongo)
        layout2.addStretch(1)
        layout2.setContentsMargins(0,1,0,1)

        self.window.resize(850, 100)
        self.window.closeEvent = self.main.closeDialog

    def apply_change(self):
        self.main.display()
        
    def nav_go(self):
        year = int(self.c_yea.currentText())
        month = int(self.c_mon.currentText())
        day = int(self.c_day.currentText())
        hour = int(self.c_hou.currentText())
        minute = int(self.c_min.currentText())
        second = int(self.c_sec.currentText())
        current_date = datetime(year,month,day,hour,minute,second)
        for i,l in enumerate(self.main.lines):
            if l.date:
                if l.date >= current_date:
                    break
        self.main.view_current = i
        self.main.display()
        self.close()

class CfgDlg:
    def __init__(self,main):
        self.main = main
        self.window = QtGui.QDialog(main)
        self.window.show()
        self.window.setWindowTitle("WhatLog")
        self.window.setWindowFlags( QtCore.Qt.Dialog | QtCore.Qt.WindowStaysOnTopHint )
        self.type_ = "cf"

        dir_ = os.path.dirname(os.path.realpath(__file__))+"/cfg/"
        files = [ os.path.splitext(f)[0] for f in os.listdir(dir_) if os.path.isfile(os.path.join(dir_,f))]# and os.path.splitext(f)[1]=="cfg"]
        
        self.buttonload = QtGui.QPushButton("load the config from:",self.window)
        self.buttonload.show()
        self.buttonload.clicked.connect(self.cfg_load)
        
        self.combobox = QtGui.QComboBox(self.window)
        self.combobox.show()
        for f in files:
            self.combobox.addItem(f)
        self.combobox.setCurrentIndex(-1)

        self.buttonsave = QtGui.QPushButton("save the config under the name:",self.window)
        self.buttonsave.show()
        self.buttonsave.clicked.connect(self.cfg_save)

        nname = self.main.current_cfgfn
        if not self.main.current_cfgfn:
            i = 1
            nname = "New_%03i"%i
            while nname in files:
                i += 1
                nname = "New_%03i"%i
        
        self.edit = QtGui.QLineEdit(self.window)
        self.edit.show()
        self.edit.setText(nname)
        
        layout1 = QtGui.QHBoxLayout()
        layout1.addWidget(self.buttonload,1)
        layout1.addWidget(self.combobox,2)
        layout2 = QtGui.QHBoxLayout()
        layout2.addWidget(self.buttonsave,1)
        layout2.addWidget(self.edit,2)

        layout3 = QtGui.QVBoxLayout(self.window)
        layout3.setContentsMargins(1,1,1,1)
        #layout3.setSpacing(0)
        layout3.addLayout(layout1)
        layout3.addLayout(layout2)
        layout3.addStretch(1)

        self.window.resize(850, 200)
        self.window.closeEvent = self.main.closeDialog

    def cfg_load(self):
        fn = self.combobox.currentText()
        filename = os.path.dirname(os.path.realpath(__file__))+"/cfg/"+fn+".cfg"
        self.main.cfgload(filename)
        self.window.close()
        
    def cfg_save(self):
        fn = self.edit.text()
        if not fn:
            return
        filename = os.path.dirname(os.path.realpath(__file__))+"/cfg/"+fn+".cfg"
        self.main.cfgsave(filename)
        self.window.close()

    def run(self):
        self.window.exec_()

            
if __name__ == '__main__':
    
    app = QtGui.QApplication(sys.argv)
    thingy = WhatLog(app)
    thingy.show()
    thingy.activateWindow()
    thingy.raise_()
    thingy.load_file()
    
    sys.exit(app.exec_())

